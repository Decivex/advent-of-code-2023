use std::collections::HashSet;
use std::io::stdin;
use crate::Direction::{East, North, South, West};

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
struct Coords(isize, isize);

impl Coords {
    fn x(&self) -> usize {
        self.0 as usize
    }

    fn y(&self) -> usize {
        self.1 as usize
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West
}

impl Direction {
    fn reverse(&self) -> Self {
        match self {
            North => South,
            East => West,
            South => North,
            West => East
        }
    }

    fn offset(&self, coords: &Coords) -> Coords {
        let (x_offset, y_offset) = match self {
            North => (0, -1),
            East => (1, 0),
            South => (0, 1),
            West => (-1, 0)
        };
        Coords(coords.0 + x_offset, coords.1 + y_offset)
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Tile {
    Vertical,
    Horizontal,
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest,
    Start,
    None
}

impl TryFrom<char> for Tile {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        Ok(match value {
            '|' => Tile::Vertical,
            '-' => Tile::Horizontal,
            'L' => Tile::NorthEast,
            'J' => Tile::NorthWest,
            'F' => Tile::SouthEast,
            '7' => Tile::SouthWest,
            'S' => Tile::Start,
            '.' => Tile::None,
            _ => Err(())?
        })
    }
}

impl Tile {
    fn char(&self) -> char {
        match self {
            Tile::Vertical => '║',
            Tile::Horizontal => '═',
            Tile::NorthEast => '╚',
            Tile::NorthWest => '╝',
            Tile::SouthEast => '╔',
            Tile::SouthWest => '╗',
            Tile::Start => 'S',
            Tile::None => '░'
        }
    }

    fn exits(&self) -> &[Direction] {
        match self {
            Tile::Vertical => &[North, South],
            Tile::Horizontal => &[East, West],
            Tile::NorthEast => &[North, East],
            Tile::NorthWest => &[North, West],
            Tile::SouthEast => &[South, East],
            Tile::SouthWest => &[South, West],
            Tile::Start => &[North, East, South, West],
            Tile::None => &[]
        }
    }
}

type Grid<T> = Vec<Vec<T>>;

fn find<T: Eq>(grid: &Grid<T>, value: &T) -> Option<Coords> {
    grid.iter().enumerate().find_map(|(row_idx, row)| {
        row.iter().position(|t| t==value).map(|col| Coords(col as isize, row_idx as isize))
    })
}

fn get<'a, T>(grid: &'a Grid<T>, coords: &Coords) -> Option<&'a T> {
    grid.get(coords.y()).iter().flat_map(|row| row.get(coords.x())).next()
}

fn valid_connections(grid: &Grid<Tile>, coords: &Coords) -> Vec<Direction> {
    get(grid, coords).iter().flat_map(|t| {
        t.exits().iter().filter(|d| {
            let next_coords = d.offset(coords);
            get(grid, &next_coords)
                .map(|u| u.exits().contains(&d.reverse()))
                .unwrap_or(false)
        })
    }).copied().collect()
}

fn count_row(row: &[Tile], y: isize, loop_tiles: &HashSet<Coords>, inner_tiles: &mut HashSet<Coords>) {
    let mut up = false;
    let mut down = false;

    for (x, tile) in row.iter().enumerate() {
        let coords = Coords(x as isize, y);

        let loop_tile = loop_tiles.contains(&coords);

        if !loop_tile && up && down {
            inner_tiles.insert(coords);
        }

        if loop_tile {
            let exits = tile.exits();

            up = up != exits.contains(&North);
            down = down != exits.contains(&South);
        }
    }
}

fn main() {
    let map: Grid<Tile> = stdin().lines().flatten()
        .map(|line| {
            line.chars().map(|c| Tile::try_from(c).unwrap()).collect::<Vec<_>>()
        })
        .collect();

    let starting_location = find(&map, &Tile::Start).unwrap();
    println!("Starting position: {:?}", starting_location);

    let mut coords = starting_location;
    let mut direction: Option<Direction> = None;
    let mut count = 0;
    let mut path_tiles: HashSet<Coords> = HashSet::new();

    loop {
        let next_direction = valid_connections(&map, &coords).into_iter()
            .filter(|d| Some(d.reverse()) != direction).next();
        direction = next_direction;

        coords = direction.unwrap().offset(&coords);

        count += 1;
        path_tiles.insert(coords);

        if coords == starting_location {
            break;
        }
    }

    let mut inner_tiles = HashSet::new();
    for (y, row) in map.iter().enumerate() {
        count_row(row.as_slice(), y as isize, &path_tiles, &mut inner_tiles);
    }

    map.iter().enumerate().for_each(|(y, row)| {
        let line = row.iter().enumerate().map(|(x, t)| {
            let c = &Coords(x as isize, y as isize);
            let style = if path_tiles.contains(c) {
                ansi_term::Color::Yellow
            } else if inner_tiles.contains(c) {
                ansi_term::Color::Cyan
            } else {
                ansi_term::Color::White
            };
            style.paint(t.char().to_string()).to_string()
        }).collect::<String>();
        println!("{line}");
    });

    println!("Circumference: {}", count / 2);
    println!("Area: {}", inner_tiles.len());
}
