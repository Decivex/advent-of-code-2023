use std::collections::BTreeSet;
use std::env;
use std::io::stdin;
use itertools::Itertools;

fn find_gaps(slice: &[usize]) -> BTreeSet<usize> {
    slice.windows(2)
        .flat_map(|c| (c[0]+1)..c[1])
        .collect()
}

fn main() {
    let scale = env::args().skip(1).next().unwrap().parse::<usize>().unwrap();

    let stars: Vec<_> = stdin().lines().flatten()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, c)| *c=='#')
                .map(|(x, _)| (x, y))
                .collect::<Vec<_>>()
        })
        .collect();

    let mut x_coords: Vec<usize> = stars.iter().map(|(x, _)| *x).collect();
    x_coords.sort();
    let mut y_coords: Vec<usize> = stars.iter().map(|(_, y)| *y).collect();
    y_coords.sort();

    let x_gaps = find_gaps(&x_coords);
    let y_gaps = find_gaps(&y_coords);

    let expanded: Vec<_> = stars.iter().map(|(x, y)| {
        let x_offset = x_gaps.range(..x).count() * (scale - 1);
        let y_offset = y_gaps.range(..y).count() * (scale - 1);
        (x+x_offset, y+y_offset)
    }).collect();

    let result: usize = expanded.iter().combinations(2)
        .map(|pair| (pair[0], pair[1]))
        .map(|(a, b)| a.0.abs_diff(b.0) + a.1.abs_diff(b.1))
        .sum();

    println!("{result}")

}
