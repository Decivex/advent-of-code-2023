use std::collections::HashMap;
use std::fmt::{Debug, Formatter, Write};
use std::io::stdin;
use crate::Cell::*;

#[derive(Eq, PartialEq, Copy, Clone)]
enum Cell {
    Full,
    Unknown,
    Empty
}

impl Debug for Cell {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Full => '#',
            Unknown => '?',
            Empty => '_'
        })
    }
}

fn parse_line(line: String) -> (Vec<Cell>, Vec<usize>) {
    let (field, constraints) = line.split_once(' ').unwrap();
    let constraints: Vec<usize> = constraints.split(',').map(|n| n.parse().unwrap()).collect();

    let constraints = constraints.repeat(5);
    let field = [field].repeat(5).join("?");

    let mut field: Vec<Cell> = field.chars().map(|c| match c {
        '.' => Empty,
        '#' => Full,
        '?' => Unknown,
        _ => panic!("Invalid character '{c}'")
    }).collect();

    field.push(Empty);

    (field, constraints)
}

fn main() {
    let verified_results: HashMap<usize, usize> = HashMap::from([
        (11, 8312508360)
    ]);

    let result: usize = stdin().lines().flatten()
        .map(parse_line)
        .enumerate()
        .map(|(idx, (field, constraints))| {
            println!("Checking pattern #{}: {field:?} with constraints {constraints:?}", idx+1);
            let solutions = solve(&field, &constraints, &mut HashMap::new());
            println!("Found {solutions} solutions\n\n");
            if let Some(verified_result) = verified_results.get(&idx) {
                if verified_result != &solutions {
                    panic!("Wrong solution for pattern #{}: Expected {verified_result}, got {solutions}", idx+1);
                } else {
                    println!("Verified result #{} checked", idx+1)
                }
            }
            solutions
        })
        .sum();
    println!("{result}")
}

fn repr_state(slice: &[Cell], constraints: &[usize]) -> String {
    let mut s = String::new();

    for tile in slice {
        write!(s, "{tile:?}").unwrap();
    }

    for constraint in constraints {
        write!(s, ",{constraint}").unwrap();
    }

    s
}

fn check_constraint(slice: &[Cell], constraint: usize) -> bool {
    if slice.len() < constraint {
        return false;
    }

    if slice.get(constraint) == Some(&Full) {
        return false;
    }

    slice[..constraint].iter().all(|c| c == &Full || c == &Unknown)
}

fn solve(cells: &[Cell], constraints: &[usize], cache: &mut HashMap<String, usize>) -> usize {
    let r = repr_state(cells, constraints);
    if let Some(r) = cache.get(&r) {
        return *r;
    }


    let result = if constraints.is_empty() {
        if cells.contains(&Full) {
            0
        } else {
            1
        }
    } else {
        let constraint = constraints[0];
        let mut sum = 0;

        for offset in 0..cells.len() {
            let slice = &cells[offset..];

            if slice[0] == Empty {
                continue;
            }

            if slice.len() < constraint {
                break;
            }

            if check_constraint(slice, constraint) {
                sum += solve(&slice[constraint+1..], &constraints[1..], cache);
            }

            if slice[0] == Full {
                break;
            }
        }

        sum
    };

    cache.insert(r, result);

    return result;
}
