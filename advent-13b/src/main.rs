use std::io::stdin;
use std::ops::BitXor;
use itertools::Itertools;

fn parse_line(s: String) -> Vec<u32> {
    s.chars().map(|c| match c {
        '.' => 0,
        '#' => 1,
        _ => panic!("Unexpected character '{c}'")
    }).collect()
}

fn find_reflection_point(slice: &[u32]) -> Option<usize> {
    for slice_point in 1..(slice.len()) {
        let left = &slice[..slice_point];
        let right = &slice[slice_point..];
        if right.iter().zip(left.iter().rev()).map(|(a, b)| a.bitxor(b).count_ones()).sum::<u32>() == 1 {
            return Some(slice_point);
        }
    }
    None
}

fn main() {
    let result: usize = stdin().lines().flatten()
        .group_by(|line| line.is_empty())
        .into_iter()
        .filter(|&(empty, _)| !empty)
        .enumerate()
        .map(|(i, (_, group))| {
            println!("Processing pattern #{}...", i+1);
            let lines = group.into_iter().map(parse_line).collect_vec();

            let rows = lines.iter().map(|row| {
                row.iter().copied()
                    .fold(0, |a, b| (a<<1)|b)
            }).collect_vec();

            let cols = (0..lines[0].len()).map(move |idx| {
                lines.iter().map(move |lines| lines.get(idx).unwrap())
                    .fold(0, |a, b| (a<<1)|b)
            }).collect_vec();

            find_reflection_point(&cols)
                .or(find_reflection_point(&rows).map(|n| n *100))
                .unwrap()

        })
        .sum();
    println!("Result: {result}");
}
