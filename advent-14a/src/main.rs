use std::collections::BTreeMap;
use std::io::stdin;

fn main() {
    let mut lines = stdin().lines().flatten().enumerate().peekable();

    let mut rock_map: Vec<BTreeMap<usize, usize>> = lines.peek().unwrap().1.chars().map(|_| {
        BTreeMap::from([(0, 0)])
    }).collect();

    let mut line_count = 0;

    for (line_idx, line) in lines {
        line_count += 1;
        for (char_idx, char) in line.chars().enumerate() {
            match char {
                '#' => {
                    rock_map[char_idx].insert(line_idx+1, 0);
                }
                'O' => {
                    *rock_map[char_idx].values_mut().next_back().unwrap()+= 1;
                },
                _ => {}
            }
        }
    }

    let result: usize = rock_map.iter().flat_map(|col| col.iter()).map(|(row, count)| {
        calculate_load(*row, line_count, *count)
    }).sum();

    println!("{result}");
}

fn calculate_load(row: usize, total_rows: usize, count: usize) -> usize {
    (0..count).map(|idx| {
        let row = row + idx;
        total_rows - row
    }).sum()
}
