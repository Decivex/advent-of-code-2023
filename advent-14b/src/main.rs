use std::collections::BTreeMap;
use std::fmt::{Display, Formatter};
use std::io::{stdin, stdout};
use std::{env, thread};
use std::time::Duration;
use ansi_term::Color;
use bimap::BiHashMap;
use crossterm::terminal::ClearType;
use Direction::*;
use crate::Object::{Block, Rock};

enum Direction {
    North,
    East,
    South,
    West
}

impl Direction {
    fn reverse(&self) -> Direction {
        match self {
            North => South,
            East => West,
            South => North,
            West => East
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Hash)]
enum Object {
    Rock,
    Block
}

impl Object {
    fn color(&self) -> Color {
        match self {
            Rock => Color::RGB(191, 63, 31),
            Block => Color::RGB(63, 63, 127)
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Hash)]
struct Coords(isize, isize);

impl Coords {
    fn offset(&self, direction: &Direction) -> Coords {
        match direction {
            North => Coords(self.0, self.1-1),
            East => Coords(self.0+1, self.1),
            South => Coords(self.0, self.1+1),
            West => Coords(self.0-1, self.1),
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Hash)]
struct Dimensions(usize, usize);

impl Dimensions {
    fn contains(&self, coords: &Coords) -> bool {
        coords.0 < self.0 as isize && coords.1 < self.1 as isize
    }
}

type ItemMap = BTreeMap<Coords, Object>;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Clone)]
struct Grid {
    items: ItemMap,
    dimensions: Dimensions
}

impl Grid {
    fn sweep_coords(&self, main_axis_offset: usize, cross_axis_offset: usize, direction: &Direction) -> Option<Coords> {
        let coords = match direction {
            North => Coords(
                cross_axis_offset as isize,
                (self.dimensions.1 - main_axis_offset - 1) as isize
            ),
            East => Coords(
                main_axis_offset as isize,
                cross_axis_offset as isize
            ),
            South => Coords(
                cross_axis_offset as isize,
                main_axis_offset as isize
            ),
            West => Coords(
                (self.dimensions.0 - main_axis_offset - 1) as isize,
                cross_axis_offset as isize
            )
        };

        if self.dimensions.contains(&coords) {
            Some(coords)
        } else {
            None
        }
    }

    fn set(&mut self, coords: Coords, object: Object) {
        self.items.insert(coords, object);
    }

    fn get(&self, coords: &Coords) -> Option<&Object> {
        self.items.get(coords)
    }
}

impl Display for Grid {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for y in (0..self.dimensions.1).step_by(2) {
            for x in 0..self.dimensions.0 {
                let upper = self.items.get(&Coords(x as isize, y as isize)).map(Object::color);
                let lower = self.items.get(&Coords(x as isize, (y + 1) as isize)).map(Object::color);
                write!(f, "{}", halfblock(upper, lower))?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}

fn halfblock(upper: Option<Color>, lower: Option<Color>) -> String {
    match (upper, lower) {
        (None, None) => " ".to_string(),
        (Some(c), None) => c.paint("▀").to_string(),
        (None, Some(c)) => c.paint("▄").to_string(),
        (Some(u), Some(d)) => d.on(u).paint("▄").to_string()
    }
}

fn main() {
    let delay = env::args().skip(1).next().map(|s| s.parse::<u64>()).into_iter().flatten().next().unwrap_or(10);

    let lines: Vec<String> = stdin().lines().flatten().collect();

    let dimensions = Dimensions(lines[0].len(), lines.len());

    let grid_items: ItemMap = lines.iter().enumerate().flat_map(|(y, row)| {
        row.chars().enumerate().filter_map(move |(x, c)| match c {
            'O' => Some(Rock),
            '#' => Some(Block),
            _ => None
        }.map(|o| (Coords(x as isize, y as isize), o)))
    }).collect();

    let mut grid = Grid { items: grid_items, dimensions };

    print_grid(&grid, "Initial state", delay);

    let mut seen_grids = BiHashMap::new();

    for cycle_idx in 0.. {
        grid = cycle(&grid, &format!("Cycle {cycle_idx:4}"), delay);

        if let Some(prev) = seen_grids.get_by_left(&grid) {
            println!("Cycle {cycle_idx} is equal to {prev}");
            let mcl = cycle_idx - prev;
            println!("Meta cycle lenght: {mcl}");

            let distance_to_end = 999_999_999 - cycle_idx;
            let final_grid = seen_grids.get_by_right(&((distance_to_end % mcl) + prev)).unwrap();
            let load = calculate_load(&final_grid);

            print_grid(final_grid, &format!("Final state (load: {load})"), delay);

            break;
        }

        seen_grids.insert(grid.clone(), cycle_idx);
    }


}

fn print_grid(grid: &Grid, status: &str, delay: u64) {
    crossterm::execute!(
        stdout(),
        crossterm::terminal::Clear(ClearType::All),
        crossterm::cursor::MoveTo(0, 0),
        crossterm::style::Print(format!("{status}\n{grid}"))
    ).unwrap();
    thread::sleep(Duration::from_millis(delay));
}

fn cycle(grid: &Grid, status: &str, delay: u64) -> Grid {
    let mut grid = grid.clone();
    for dir in [North, West, South, East] {
        grid = apply_gravity(&grid, &dir);
        print_grid(&grid, status, delay);
    }
    return grid;
}

fn apply_gravity(grid: &Grid, direction: &Direction) -> Grid {
    let mut next: Grid = Grid {
        items: grid.items.clone().into_iter().filter(|(_, it)| it==&Block).collect(),
        dimensions: grid.dimensions
    };

    let sweep_dir = direction.reverse();

    let (main_axis_len, cross_axis_len) = match sweep_dir {
        North|South => (grid.dimensions.1, grid.dimensions.0),
        East|West => (grid.dimensions.0, grid.dimensions.1)
    };

    let mut sweep_line: Vec<Coords> = (0..cross_axis_len).map(|c| grid.sweep_coords(0, c, &sweep_dir).unwrap()).collect();

    for main_coord in 0..main_axis_len {
        for cross_coord in 0..cross_axis_len {
            let coords = grid.sweep_coords(main_coord, cross_coord, &sweep_dir).unwrap();

            match grid.get(&coords) {
                None => {}
                Some(Rock) => {
                    next.set(sweep_line[cross_coord], Rock);
                    sweep_line[cross_coord] = sweep_line[cross_coord].offset(&sweep_dir);
                }
                Some(Block) => {
                    sweep_line[cross_coord] = coords.offset(&sweep_dir);
                }
            }
        }
    }

    next
}

fn calculate_load(grid: &Grid) -> usize {
    let mut total_load = 0;
    for y in 0..grid.dimensions.1 {
        let row_load = grid.dimensions.1 - y;
        for x in 0..grid.dimensions.0 {
            if grid.get(&Coords(x as isize, y as isize)) == Some(&Rock) {
                total_load += row_load;
            }
        }
    }
    total_load
}