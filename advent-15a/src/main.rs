use std::io::stdin;

fn main() {
    let result: u32 = stdin().lines().next().unwrap().unwrap().split(',').map(hash).sum();
    println!("{result}");
}

fn hash(string: &str) -> u32 {
    let mut hash: u32 = 0;
    for byte in string.bytes() {
        hash = ((hash+ byte as u32) * 17) % 256;
    }
    return hash;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash() {
        assert_eq!(hash("HASH"), 52);
    }

    #[test]
    fn test_input() {
        let test_input = include_str!("../test_input.txt");
        assert_eq!(test_input.split(',').map(hash).sum::<u32>(), 1320)
    }
}