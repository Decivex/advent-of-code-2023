use std::io::stdin;

#[derive(Debug)]
struct Lens(String, u8);

#[derive(Debug)]
struct Instruction(String, Action);

#[derive(Debug)]
enum Action {
    Remove,
    Set(u8)
}

fn main() {
    let mut boxes: Vec<Vec<Lens>> = (0..256).map(|_| Vec::new()).collect();

    for instruction in stdin().lines().next().unwrap().unwrap().split(',').map(parse_instruction) {
        let box_ = boxes.get_mut(hash(&instruction.0) as usize).unwrap();

        match instruction.1 {
            Action::Remove => {
                if let Some(index) = box_.iter().position(|l| l.0 == instruction.0) {
                    box_.remove(index);
                }
            }
            Action::Set(value) => {
                match box_.iter_mut().filter(|l| l.0==instruction.0).next() {
                    None => box_.push(Lens(instruction.0, value)),
                    Some(lens) => lens.1 = value
                }
            }
        }
    };

    let result: usize = boxes.iter().enumerate().flat_map(|(box_idx, box_)| {
        box_.iter().enumerate().map(move |(lens_idx, lens)| {
            (box_idx + 1) * (lens_idx + 1) * lens.1 as usize
        })
    }).sum();
    println!("{result}")
}

fn parse_instruction(str: &str) -> Instruction {
    if let Some((label, lens)) = str.split_once('=') {
        Instruction(label.to_string(), Action::Set(lens.parse().unwrap()))
    } else {
        Instruction(str.strip_suffix('-').unwrap().to_string(), Action::Remove)
    }
}

fn hash(string: &str) -> u32 {
    let mut hash: u32 = 0;
    for byte in string.bytes() {
        hash = ((hash+ byte as u32) * 17) % 256;
    }
    return hash;
}
