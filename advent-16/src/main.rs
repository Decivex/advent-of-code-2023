use std::collections::{HashMap, HashSet};
use std::io::stdin;
use std::ops::Range;

type Vector = [isize; 2];

enum Diagonal {
    Major,
    Minor
}

impl Diagonal {
    fn reflect(&self, v: &Vector) -> Vector {
        match self {
            Diagonal::Major => [v[1], v[0]],
            Diagonal::Minor => [-v[1], -v[0]]
        }
    }
}

#[derive(Eq, PartialEq)]
enum Orientation {
    Horizontal,
    Vertical
}

enum Object {
    Mirror(Diagonal),
    Splitter(Orientation)
}

impl Object {
    fn get_next(&self, position: &Vector, direction: &Vector) -> Vec<(Vector, Vector)> {
        match self {
            Object::Mirror(diagonal) => {
                let new_direction = diagonal.reflect(direction);
                let new_position = add_vectors(position, &new_direction);
                vec![
                    (new_position, new_direction)
                ]
            }
            Object::Splitter(orientation) => {
                if &vector_orientation(direction).unwrap() == orientation {
                    vec![
                        (add_vectors(position, direction), direction.clone())
                    ]
                } else {
                    let d1 = Diagonal::Major.reflect(direction);
                    let d2 = Diagonal::Minor.reflect(direction);
                    vec![
                        (add_vectors(position, &d1), d1),
                        (add_vectors(position, &d2), d2)
                    ]
                }
            }
        }
    }
}

struct Bounds(Range<isize>, Range<isize>);

impl Bounds {
    fn contains(&self, v: &Vector) -> bool {
        self.0.contains(&v[0]) && self.1.contains(&v[1])
    }
}

fn add_vectors(a: &Vector, b: &Vector) -> Vector {
    [a[0]+b[0],a[1]+b[1]]
}

fn vector_orientation(v: &Vector) -> Option<Orientation> {
    match v {
        [0, 0] => None,
        [0, _] => Some(Orientation::Vertical),
        [_, 0] => Some(Orientation::Horizontal),
        [_, _] => None
    }
}

fn parse_lines(lines: &[String]) -> HashMap<Vector, Object> {
    lines.iter().enumerate().flat_map(|(y, line)| {
        line.chars().enumerate().filter_map(move |(x, c)| Some(([x as isize, y as isize], match c {
            '\\' => Object::Mirror(Diagonal::Major),
            '/' => Object::Mirror(Diagonal::Minor),
            '-' => Object::Splitter(Orientation::Horizontal),
            '|' => Object::Splitter(Orientation::Vertical),
            '.' => None?,
            _ => panic!("Unexpected character '{c}'")
        })))
    }).collect()
}

fn main() {
    let lines: Vec<String> = stdin().lines().flatten().collect();
    let width = lines[0].len() as isize;
    let height = lines.len() as isize;
    let bounds = Bounds(0..width, 0..height);
    let objects = parse_lines(&lines);

    let left_params = bounds.1.clone().map(|y| ([0, y], [1isize, 0]));
    let right_params = bounds.1.clone().map(|y| ([bounds.1.end-1, y], [-1isize, 0]));
    let top_params = bounds.0.clone().map(|x| ([x, 0], [0isize, 1]));
    let bottom_params = bounds.0.clone().map(|x| ([x, bounds.1.end-1], [0isize, -1]));

    let max_power = left_params.chain(right_params).chain(top_params).chain(bottom_params).map(|(start, direction)| {
        let power = calculate_power(&objects, &bounds, &start, &direction);
        println!("Start position:\t{start:?}\tDirection:\t{direction:?}\t POWER:\t{power}");
        power
    }).max().unwrap();

    println!("Max power:\t{max_power}");
}

fn calculate_power(objects: &HashMap<Vector, Object>, bounds: &Bounds, start_location: &Vector, direction: &Vector) -> usize {
    let mut frontier: Vec<(Vector, Vector)> = Vec::from([
        (start_location.clone(), direction.clone())
    ]);

    let mut crossed_boundaries: HashSet<(Vector, Vector)> = HashSet::new();

    while let Some((position, direction)) = frontier.pop() {

        for (next_position, next_direction) in objects.get(&position)
            .map(|o| o.get_next(&position, &direction))
            .unwrap_or_else(|| vec![(add_vectors(&position, &direction), direction)]) {
            let boundary = (position, next_position);

            if bounds.contains(&next_position) && !crossed_boundaries.contains(&boundary) {
                frontier.push((next_position, next_direction));
                crossed_boundaries.insert(boundary);
            }
        }
    }

    let visited_tiles: HashSet<Vector> = crossed_boundaries.iter().flat_map(|b| [b.0, b.1]).collect();
    return visited_tiles.len();
}
