use std::collections::{BTreeSet, HashMap};
use std::io::stdin;
use std::iter::successors;
use std::ops::{Add, Range};
use ansi_term::Color;

const DIRECTIONS: &[Vector] = &[
    Vector(0, -1),
    Vector(1, 0),
    Vector(0, 1),
    Vector(-1, 0)
];

#[derive(Debug, Hash, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
struct Vector(isize, isize);

impl Add<Vector> for Vector {
    type Output = Vector;

    fn add(self, rhs: Vector) -> Self::Output {
        Vector(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Vector {
    fn reverse(&self) -> Vector {
        Vector(-self.0, -self.1)
    }

}

struct Bounds(Range<isize>, Range<isize>);

impl Bounds {
    fn contains(&self, v: &Vector) -> bool {
        self.0.contains(&v.0) && self.1.contains(&v.1)
    }
}

#[derive(Debug, Hash, Ord, PartialOrd, Eq, PartialEq, Clone)]
struct State {
    location: Vector,
    direction: Vector,
    distance: usize,
}

fn crucible_valid(state: &State, prev: &State, bounds: &Bounds) -> bool {
    bounds.contains(&state.location) &&
    state.distance <= 2 &&
    state.direction != prev.direction.reverse()
}

fn ultra_crucible_valid(state: &State, prev: &State, bounds: &Bounds) -> bool {
    if !bounds.contains(&state.location) || state.direction == prev.direction.reverse() {
        return false;
    }

    if state.distance >= 10 {
        return false;
    }

    if prev.distance < 3 && state.direction != prev.direction && prev.direction != Vector(0, 0) {
        return false;
    }

    if state.distance < 4 && state.location == Vector(bounds.0.end-1, bounds.1.end-1) {
        return false;
    }

    return true;
}

impl State {
    fn next_states(&self, bounds: &Bounds, state_predicate: fn(&State, &State, &Bounds) -> bool) -> Vec<State> {
        DIRECTIONS.iter()
            .map(|d| {
                State {
                    location: self.location + *d,
                    direction: d.clone(),
                    distance: if d == &self.direction {
                        self.distance + 1
                    } else {
                        0
                    },
                }
            })
            .filter(|s| state_predicate(&s, &self, &bounds))
            .collect()
    }
}


fn main() {
    let (bounds, costs) = load_data();

    let crucible_path = get_path(&costs, &bounds, crucible_valid);
    let ultra_crucible_path = get_path(&costs, &bounds, ultra_crucible_valid);

    for y in bounds.1 {
        let line: String = bounds.0.clone().map(|x| {
            let v = Vector(x, y);

            let c = 255 - (costs[&v] as u8) * 15;

            let (fg, bg) = match (crucible_path.contains(&v), ultra_crucible_path.contains(&v)) {
                (false, false) => (Color::Black, Color::RGB(c, c, c)),
                (true, false) => (Color::Yellow, Color::RGB(0, 0, c)),
                (false, true) => (Color::Yellow, Color::RGB(c, 0, 0)),
                (true, true) => (Color::Yellow, Color::RGB(c / 2, 0, c / 2))
            };

            fg.on(bg).paint(costs[&v].to_string()).to_string()
        }).collect();
        println!("{line}");
    }
    let cost: u32 = crucible_path.iter().rev().skip(1).map(|v| costs[v]).sum();
    let ultra_cost: u32 = ultra_crucible_path.iter().rev().skip(1).map(|v| costs[v]).sum();
    println!("Crucible heat loss: {cost}");
    println!("ULTRA crucible heat loss: {ultra_cost}");
}

fn get_path(costs: &HashMap<Vector, u32>, bounds: &Bounds, state_predicate: fn(&State, &State, &Bounds) -> bool) -> Vec<Vector> {
    let initial_state = State {
        location: Vector(0, 0),
        direction: Vector(0, 0),
        distance: 0
    };

    let mut total_costs = HashMap::from([(initial_state.clone(), 0u32)]);
    let mut queue = BTreeSet::from([(0u32, initial_state)]);
    let mut source: HashMap<State, State> = HashMap::new();

    while let Some((current_cost, s)) = queue.pop_first() {
        for nxt in s.next_states(&bounds, state_predicate).iter() {
            let next_cost = current_cost + costs[&nxt.location];
            let replace = total_costs.get(nxt).map(|c| next_cost < *c).unwrap_or(true);

            if replace {
                total_costs.insert(nxt.clone(), next_cost);
                source.insert(nxt.clone(), s.clone());
                queue.insert((next_cost, nxt.clone()));
            }
        }
    }

    let end_point = Vector(bounds.0.end-1, bounds.1.end-1);
    let r = total_costs.iter()
        .filter(|(s, _)| s.location == end_point)
        .min_by_key(|(_, c)| *c);

    successors(r.map(|p|p.0), |s| source.get(&s))
        .map(|s| s.location)
        .collect()
}

fn load_data() -> (Bounds, HashMap<Vector, u32>) {
    let lines: Vec<String> = stdin().lines().flatten().collect();
    let bounds = Bounds(0..lines[0].len() as isize, 0..lines.len() as isize);
    let costs: HashMap<Vector, u32> = lines.iter()
        .enumerate()
        .flat_map(|(y, line)| line.chars()
            .enumerate()
            .map(move |(x, d)| (Vector(x as isize, y as isize), d.to_digit(10).unwrap())))
        .collect();

    return (bounds, costs);
}