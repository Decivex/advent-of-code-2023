pub mod rect;
pub mod vector;
pub mod svg;

use itertools::Itertools;
use crate::common::rect::Rect;
use crate::common::svg::{Element, Fill, Namespace, Style, SVG, ViewBox};
use crate::common::vector::Vector;

#[derive(Debug)]
pub struct Instruction(pub Vector, pub String);

pub fn parse_instruction(s: String) -> Option<Instruction> {
    let (v, c) = s.rsplit_once(' ')?;
    Some(Instruction(parse_vector(v), parse_color(c)?))
}

pub fn parse_color(s: &str) -> Option<String> {
    Some(s.strip_prefix("(")?.strip_suffix(")")?.to_string())
}

pub fn parse_vector(s: &str) -> Vector {
    let (d, l) = s.split_once(' ').unwrap();

    let direction = parse_direction(d.chars().next().unwrap());
    let magnitude: isize = l.parse().unwrap();

    return direction * magnitude;
}

pub fn parse_direction(c: char) -> Vector {
    match c {
        'U' => Vector(0, -1),
        'D' => Vector(0, 1),
        'L' => Vector(-1, 0),
        'R' => Vector(1, 0),
        _ => panic!("Unexpected character '{c}'")
    }
}

pub fn expand_polygon(vertices: &mut [Vector]) {
    for idx in 0..(vertices.len()-1) {
        let s = &mut vertices[idx..idx+2];
        let offset = tangent(&s[0], &s[1]).sign().max_coords(Vector::default());
        s[0] = &s[0] + offset;
        s[1] = &s[1] + offset;
    }
}

pub fn generate_outline<'a, V, C>(vertices: V, colors: C) -> Vec<Element> where V: Iterator<Item=&'a Vector>, C: Iterator<Item=&'a str> {
    vertices.tuple_windows().zip(colors).map(|((a, b), color)| {
        Element::Line {
            x1: a.0.into(),
            y1: a.1.into(),
            x2: b.0.into(),
            y2: b.1.into(),
            style: Style {
                fill: Some(Fill::None),
                stroke: Some(color.to_string()),
                stroke_width: Some(0.3.into()),
            },
        }
    }).collect()
}

pub fn tangent(a: &Vector, b: &Vector) -> Vector {
    let delta = b - a.clone();
    Vector(delta.1, -delta.0)
}


pub fn generate_svg(path: &Vec<Vector>, lines: Vec<Element>) -> SVG {
    let rect = Rect::fit_points(&path, 1);
    let viewbox = ViewBox::from(&rect);

    let polygon = Element::Polygon {
        points: path.clone()
    };

    let outline = Element::Group {
        elements: lines,
    };

    let grid_style = Style {
        fill: Some(Fill::None),
        stroke: Some("white".into()),
        stroke_width: Some(0.03.into()),
    };

    let svg = SVG {
        namespace: Namespace,
        viewbox,
        elements: vec![
            Element::Pattern {
                id: "grid".to_string(),
                width: 1.0,
                height: 1.0,
                pattern_units: "userSpaceOnUse".to_string(),
                elements: vec![
                    Element::Polyline {
                        points: vec![
                            Vector(0, 1),
                            Vector(0, 0),
                            Vector(1, 0)
                        ],
                        style: grid_style
                    }
                ],
            },
            polygon,
            (rect, Style::fill("url(#grid)")).into(),
            outline
        ]
    };
    svg
}

pub fn calculate_polygon_area(vertices: &[Vector]) -> isize {
    vertices.iter().cycle().take(vertices.len()+1).tuple_windows().map(|(a, b)| {
        (b.0 + a.0) * (b.1 - a.1)
    }).sum::<isize>() / 2
}