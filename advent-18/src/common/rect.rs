use crate::vector::Vector;

#[derive(Debug)]
pub struct Rect(pub Vector, pub Vector);

impl Rect {
    pub fn fit_points(points: &[Vector], border: isize) -> Self {
        let border_offset = Vector::unit() * border;
        let from = &points.iter()
            .copied()
            .reduce(Vector::min_coords)
            .unwrap_or_default() - border_offset;
        let to = &points.iter()
            .copied()
            .reduce(Vector::max_coords)
            .unwrap_or_default() + border_offset;
        Self(from, to)
    }

    pub fn width(&self) -> isize {
        (&self.1 - self.0).0
    }

    pub fn height(&self) -> isize {
        (&self.1 - self.0).1
    }
}