use itertools::Itertools;
use serde::{Serialize, Serializer};
use crate::rect::Rect;
use crate::vector::Vector;

fn serialize_points<S: Serializer>(list: &Vec<Vector>, ser: S) -> Result<S::Ok, S::Error> {
    let attr = list.iter().map(|v| format!("{},{}", v.0, v.1)).join(" ");
    ser.serialize_str(&attr)
}

pub enum Unit {
    Percentage(f32),
    Points(f32),
}

impl From<isize> for Unit {
    fn from(value: isize) -> Self {
        Unit::Points(value as f32)
    }
}

impl From<f32> for Unit {
    fn from(value: f32) -> Self {
        Unit::Points(value)
    }
}

impl Serialize for Unit {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(&self.to_string())
    }
}

impl ToString for Unit {
    fn to_string(&self) -> String {
        match self {
            Unit::Percentage(val) => format!("{val}%"),
            Unit::Points(val) => format!("{val}")
        }
    }
}

pub enum Fill {
    Color(String),
    None,
}

impl ToString for Fill {
    fn to_string(&self) -> String {
        match self {
            Fill::Color(c) => c.as_str(),
            Fill::None => "none"
        }.to_string()
    }
}

pub struct Style {
    pub fill: Option<Fill>,
    pub stroke: Option<String>,
    pub stroke_width: Option<Unit>
}

impl Style {
    pub fn fill<S: AsRef<str>>(fill: S) -> Style {
        Self {
            fill: Some(Fill::Color(fill.as_ref().to_string())),
            stroke: None,
            stroke_width: None
        }
    }
}

fn format_properties(properties: &[(&str, &Option<String>)]) -> String {
    properties.iter()
        .filter(|(_, value)| value.is_some())
        .map(|(name, value)| format!("{}: {};", name, value.as_ref().unwrap()))
        .join(" ")
}

impl Serialize for Style {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(&format_properties(&[
            ("fill", &self.fill.as_ref().map(|f| f.to_string())),
            ("stroke", &self.stroke),
            ("stroke-width", &self.stroke_width.as_ref().map(|f| f.to_string())),
        ]))
    }
}

impl Default for Style {
    fn default() -> Self {
        Self {
            fill: Some(Fill::Color("black".to_string())),
            stroke: None,
            stroke_width: None
        }
    }
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub enum Element {
    Polygon {
        #[serde(serialize_with = "serialize_points", rename = "@points")]
        points: Vec<Vector>
    },
    Polyline {
        #[serde(serialize_with = "serialize_points", rename = "@points")]
        points: Vec<Vector>,
        #[serde(rename = "@style")]
        style: Style,
    },
    Pattern {
        #[serde(rename = "@id")]
        id: String,
        #[serde(rename = "@width")]
        width: f32,
        #[serde(rename = "@height")]
        height: f32,
        #[serde(rename = "@patternUnits")]
        pattern_units: String,
        #[serde(rename = "$value")]
        elements: Vec<Element>,
    },

    Rect {
        #[serde(rename = "@x")]
        x: Unit,
        #[serde(rename = "@y")]
        y: Unit,
        #[serde(rename = "@width")]
        width: Unit,
        #[serde(rename = "@height")]
        height: Unit,
        #[serde(rename = "@style")]
        style: Style,
    },

    Line {
        #[serde(rename="@x1")]
        x1: Unit,
        #[serde(rename="@y1")]
        y1: Unit,
        #[serde(rename="@x2")]
        x2: Unit,
        #[serde(rename="@y2")]
        y2: Unit,
        #[serde(rename="@style")]
        style: Style
    },

    #[serde(rename="g")]
    Group {
        #[serde(rename="$value")]
        elements: Vec<Element>
    }
}

impl From<(Rect, Style)> for Element {
    fn from(value: (Rect, Style)) -> Self {
        Element::Rect {
            x: value.0.0.0.into(),
            y: value.0.0.1.into(),
            width: (value.0.1.0 - value.0.0.0).into(),
            height: (value.0.1.1 - value.0.0.1).into(),
            style: value.1,
        }
    }
}

pub struct ViewBox(pub isize, pub isize, pub isize, pub isize);

impl ViewBox {
    pub fn fit_points(points: &[Vector], border: isize) -> Self {
        let border_offset = Vector::unit() * border;
        let from = &points.iter()
            .copied()
            .reduce(Vector::min_coords)
            .unwrap_or_default() - border_offset;
        let to = &points.iter()
            .copied()
            .reduce(Vector::max_coords)
            .unwrap_or_default() + border_offset;
        ViewBox(from.0, from.1, to.0, to.1)
    }
}

impl Serialize for ViewBox {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(&format!("{} {} {} {}", self.0, self.1, self.2, self.3))
    }
}

impl From<&Rect> for ViewBox {
    fn from(value: &Rect) -> Self {
        Self(
            value.0.0,
            value.0.1,
            value.width(),
            value.height()
        )
    }
}

pub struct Namespace;

impl Serialize for Namespace {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str("http://www.w3.org/2000/svg")
    }
}

#[derive(Serialize)]
#[serde(rename = "svg")]
pub struct SVG {
    #[serde(rename = "@xmlns")]
    pub namespace: Namespace,
    #[serde(rename = "@viewBox")]
    pub viewbox: ViewBox,
    #[serde(rename = "$value")]
    pub elements: Vec<Element>,
}