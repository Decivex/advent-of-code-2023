use std::ops::{Add, Mul, Sub};

#[derive(Debug, Eq, PartialEq, Copy, Clone, Default)]
pub struct Vector(pub isize, pub isize);

impl AsRef<Vector> for Vector {
    fn as_ref(&self) -> &Vector {
        &self
    }
}

impl Mul<isize> for Vector {
    type Output = Vector;

    fn mul(self, rhs: isize) -> Self::Output {
        Vector(self.0 * rhs, self.1 * rhs)
    }
}

impl Add<Vector> for &Vector {
    type Output = Vector;

    fn add(self, rhs: Vector) -> Self::Output {
        Vector(self.0+rhs.0, self.1+rhs.1)
    }
}

impl<V: AsRef<Vector>> Sub<V> for &Vector {
    type Output = Vector;

    fn sub(self, rhs: V) -> Self::Output {
        let rhs = rhs.as_ref();
        Vector(self.0-rhs.0, self.1-rhs.1)
    }
}

impl Vector {
    pub fn unit() -> Vector {
        Vector(1, 1)
    }

    pub fn max_coords(self, other: Vector) -> Vector {
        Vector(self.0.max(other.0), self.1.max(other.1))
    }

    pub fn min_coords(self, other: Vector) -> Vector {
        Vector(self.0.min(other.0), self.1.min(other.1))
    }

    pub fn sign(self) -> Vector {
        Self(self.0.signum(), self.1.signum())
    }
}