pub mod common;

use std::fs;
use std::io::stdin;
use std::iter::successors;
use common::*;
use crate::common::vector::Vector;

fn parse_instruction(s: String) -> Option<Instruction> {
    let (v, c) = s.rsplit_once(' ')?;
    Some(Instruction(parse_vector(v), parse_color(c)?))
}

fn main() {
    let instructions: Vec<Instruction> = stdin().lines().flatten().map(parse_instruction).flatten().collect();
    let mut iter = instructions.iter();
    let mut path: Vec<Vector> = successors(Some(Vector(0, 0)), |a| iter.next().map(|ins| a+ins.0)).collect();

    expand_polygon(&mut path);

    let lines = generate_outline(path.iter(), instructions.iter().map(|i| i.1.as_str()));

    path.pop();

    let svg = generate_svg(&path, lines);

    fs::write("part-1.svg", serde_xml_rs::to_string(&svg).unwrap()).unwrap();

    println!("{}", calculate_polygon_area(&path))
}
