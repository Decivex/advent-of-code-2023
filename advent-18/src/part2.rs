pub mod common;

use std::fs;
use std::io::stdin;
use std::iter::successors;
use common::*;
use crate::common::vector::Vector;

fn parse_instruction(s: String) -> Option<Instruction> {
    let hex = s.rsplit_once(' ')?.1
        .strip_prefix('(')?
        .strip_suffix(')')?;
    let dist: isize = isize::from_str_radix(&hex[1..6], 16).ok()?;
    let vector = match hex[6..].chars().next()?.to_digit(16)? {
        0 => Vector(1, 0),
        1 => Vector(0, 1),
        2 => Vector(-1, 0),
        3 => Vector(0, -1),
        _ => None?
    } * dist;
    Some(Instruction(vector, hex.to_string()))
}

fn main() {
    let instructions: Vec<Instruction> = stdin().lines().flatten().map(parse_instruction).flatten().collect();
    let mut iter = instructions.iter();
    let mut path: Vec<Vector> = successors(Some(Vector(0, 0)), |a| iter.next().map(|ins| a+ins.0)).collect();

    expand_polygon(&mut path);

    let lines = generate_outline(path.iter(), instructions.iter().map(|i| i.1.as_str()));

    path.pop();

    let svg = generate_svg(&path, lines);

    fs::write("part-2.svg", serde_xml_rs::to_string(&svg).unwrap()).unwrap();

    println!("{}", calculate_polygon_area(&path))
}
