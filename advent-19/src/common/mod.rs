use std::collections::BTreeMap;
use std::str::FromStr;
use utils::Between;

pub mod utils;

#[derive(Debug)]
pub struct Workflow(pub Vec<Instruction>);

impl FromStr for Workflow {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let instructions: Result<Vec<Instruction>, &'static str> = s
            .between('{', '}')
            .ok_or("no brackets")?
            .split(',')
            .map(Instruction::from_str)
            .collect();

        Ok(Workflow(instructions?))
    }
}

impl Workflow {
    pub fn process(&self, part: &Part) -> Option<&Destination> {
        self.0.iter().find(|i| {
            i.0.as_ref().map(|condition| condition.check(part.0[&condition.lhs])).unwrap_or(true)
        }).map(|i| &i.1)
    }
}

#[derive(Debug)]
pub struct Instruction(pub Option<Condition>, pub Destination);

impl FromStr for Instruction {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.split_once(':') {
            None => Ok(Instruction(None, s.parse()?)),
            Some((condition, destination)) => Ok(Instruction(
                Some(condition.parse()?),
                destination.parse()?
            )),
        }
    }
}

#[derive(Debug)]
pub struct Condition {
    pub lhs: char,
    pub rhs: i64,
    pub operator: ConditionOperator
}

impl FromStr for Condition {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.chars();
        let lhs = it.next().ok_or("No lhs operand")?;
        let operator = match it.next().ok_or("No operator")? {
            '>' => ConditionOperator::GreaterThan,
            '<' => ConditionOperator::LessThan,
            _ => Err("Invalid operator")?
        };
        let rhs: i64 = it.as_str().parse().map_err(|_| "Error parsing rhs operand")?;
        Ok(Condition { lhs, rhs, operator })
    }
}

impl Condition {
    pub fn check(&self, value: i64) -> bool {
        match self.operator {
            ConditionOperator::GreaterThan => value > self.rhs,
            ConditionOperator::LessThan => value < self.rhs
        }
    }
}

#[derive(Debug)]
pub enum ConditionOperator {
    GreaterThan,
    LessThan
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Destination {
    Workflow(String),
    Accept,
    Reject
}

impl FromStr for Destination {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "A" => Destination::Accept,
            "R" => Destination::Reject,
            _ => Destination::Workflow(s.to_string())
        })
    }
}

#[derive(Debug)]
pub struct Part(pub BTreeMap<char, i64>);

impl FromStr for Part {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let properties: Result<BTreeMap<char, i64>, &'static str> = s
            .between('{', '}').ok_or("No brackets")?
            .split(',')
            .map(parse_property)
            .collect();

        Ok(Part(properties?))
    }
}

fn parse_property(s: &str) -> Result<(char, i64), &'static str> {
    let (key, value) = s.split_once("=").ok_or("No '='")?;

    Ok((
        key.parse().ok().ok_or("Error parsing char")?,
        value.parse().ok().ok_or("Error parsing number")?
    ))
}

pub fn parse_workflow_line<S: AsRef<str>>(line: S) -> (String, Workflow) {
    let line = line.as_ref();
    let idx = line.find('{').unwrap();
    let name = line[..idx].to_string();
    let workflow: Workflow = line[idx..].parse().unwrap();
    (name, workflow)
}
