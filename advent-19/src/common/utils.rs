pub trait Between {
    fn between(&self, prefix: char, suffix: char) -> Option<&str>;
}

impl<S: AsRef<str>> Between for S {
    fn between(&self, prefix: char, suffix: char) -> Option<&str> {
        self.as_ref().strip_prefix(prefix)?.strip_suffix(suffix)
    }
}

pub trait Swap<T> {
    fn swap(self) -> Self;
}

impl<T> Swap<T> for (T, T) {
    fn swap(self) -> Self {
        (self.1, self.0)
    }
}
