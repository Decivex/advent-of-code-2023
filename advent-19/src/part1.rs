extern crate core;

use std::collections::HashMap;
use std::io::{Read, stdin};
use std::iter::successors;

pub mod common;

use common::*;

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();
    let (instructions, parts) = input.split_once("\n\n").unwrap();

    let workflows: HashMap<String, Workflow> = instructions.lines().map(parse_workflow_line).collect();

    let parts: Result<Vec<Part>, _> = parts.lines()
        .map(|line| line.parse::<Part>())
        .collect();

    let sum: i64 = parts.unwrap().iter().filter(|p| {
        let final_destination = successors(Some(Destination::Workflow("in".to_string())), |wf| {
            if let Destination::Workflow(wf) = wf {
                workflows[wf].process(p).cloned()
            } else {
                None
            }
        }).last().unwrap();
        final_destination == Destination::Accept
    }).flat_map(|p| p.0.values()).sum();

    println!("{sum}");
}
