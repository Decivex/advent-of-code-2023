pub mod common;

use std::collections::{BTreeMap, HashMap};
use std::io::stdin;
use std::ops::Range;
use common::*;
use common::utils::Swap;

type PropertyRange = Range<isize>;

fn main() {
    let workflows: HashMap<String, Workflow> = stdin().lines().flatten()
        .take_while(|line| !line.is_empty())
        .map(parse_workflow_line)
        .collect();

    let mut products = Vec::from([
        ("in".to_string(), PartRange::new())
    ]);

    let mut accepted = Vec::new();

    while let Some((workflow, pr)) = products.pop() {
        let workflow = &workflows[&workflow];
        for (pr, dst) in process_part_range(workflow, &pr) {
            match dst {
                Destination::Workflow(wf) => products.push((wf, pr)),
                Destination::Accept => accepted.push(pr),
                Destination::Reject => {}
            }
        }
    }

    let result: usize = accepted.iter().map(PartRange::count).sum();

    println!("{result}");
}

fn process_part_range(workflow: &Workflow, part_range: &PartRange) -> Vec<(PartRange, Destination)> {
    let instructions = &workflow.0;
    let mut part_range = Some(part_range.clone());
    let mut results = Vec::new();

    for ins in instructions {
        if part_range.is_none() {
            break;
        }

        let result = match &ins.0 {
            None => {
                part_range.take()
            }
            Some(condition) => {
                dbg!(condition);
                let (matched, remainder) = part_range.as_ref().unwrap().split_on(condition);
                part_range.replace(dbg!(remainder));
                dbg!(matched.count());
                dbg!(matched).existing()
            }
        };

        if let Some(p) = result {
            results.push((p, ins.1.clone()))
        }

        println!("{part_range:?}")
    }

    results
}

#[derive(Debug, Clone)]
struct PartRange(BTreeMap<char, PropertyRange>);

impl PartRange {
    fn new() -> Self {
        Self(BTreeMap::from([
            ('x', 1..4000+1),
            ('m', 1..4000+1),
            ('a', 1..4000+1),
            ('s', 1..4000+1)
        ]))
    }

    fn split_on(&self, condition: &Condition) -> (PartRange, PartRange) {
        let key = condition.lhs;
        let range = &self.0[&key];

        let (m, r) = match condition.operator {
            ConditionOperator::GreaterThan => {
                split_range(range, (condition.rhs + 1) as isize).swap()
            }
            ConditionOperator::LessThan => {
                split_range(range, condition.rhs as isize)
            }
        };

        let mut matched = self.0.clone();
        let mut remainder = self.0.clone();
        matched.insert(key, m);
        remainder.insert(key, r);

        (PartRange(matched), PartRange(remainder))
    }

    fn is_empty(&self) -> bool {
        self.0.values().any(PropertyRange::is_empty)
    }

    fn existing(self) -> Option<PartRange> {
        if self.is_empty() {
            None
        } else {
            Some(self)
        }
    }

    fn count(&self) -> usize {
        self.0.values().map(|r| ExactSizeIterator::len(r)).product()
    }
}

fn split_range(range: &PropertyRange, mid: isize) -> (PropertyRange, PropertyRange) {
    return (range.start..mid, mid..range.end);
}
