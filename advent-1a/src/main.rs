use std::io::stdin;

fn main() {
    let result: u32 = stdin()
        .lines()
        .flatten()
        .map(|line| line.chars()
            .map(|c| c.to_digit(10))
            .flatten()
            .collect::<Vec<_>>())
        .map(|digits| digits.first().unwrap() * 10 + digits.last().unwrap())
        .sum();
    println!("{result}");
}
