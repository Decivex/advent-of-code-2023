use std::io::stdin;

const DIGITS: &[(&str, u32)] = &[
    ("zero", 0), ("0", 0),
    ("one", 1), ("1", 1),
    ("two", 2), ("2", 2),
    ("three", 3), ("3", 3),
    ("four", 4), ("4", 4),
    ("five", 5), ("5", 5),
    ("six", 6), ("6", 6),
    ("seven", 7), ("7", 7),
    ("eight", 8), ("8", 8),
    ("nine", 9), ("9", 9),
];

fn parse_digit(s: &str) -> Option<u32> {
    DIGITS.iter()
        .filter(|(pattern, _)| s.starts_with(pattern))
        .map(|(_, digit)| *digit)
        .next()
}

fn find_digits(s: &str) -> Vec<u32> {
    let mut digits: Vec<u32> = Vec::new();
    let mut iter = s.chars();

    loop {
        if let Some(digit) = parse_digit(iter.as_str()) {
            digits.push(digit);
        }

        if iter.next().is_none() {
            break;
        }
    }

    return digits;
}

fn main() {
    let result: u32 = stdin().lines().flatten()
        .map(|line| find_digits(&line))
        .map(|digits| digits.first().unwrap() * 10 + digits.last().unwrap())
        .sum();
    println!("{result}");
}
