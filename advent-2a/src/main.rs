use std::collections::HashMap;
use std::io::stdin;

#[derive(Debug)]
struct Game {
    id: u32,
    samples: Vec<HashMap<String, u32>>
}

fn parse_game(line: String) -> Game {
    let (id, data) = line.strip_prefix("Game ").unwrap().split_once(':').unwrap();
    let id: u32 = id.parse().unwrap();

    let samples: Vec<HashMap<String, u32>> = data.split(';').map(|sample| {
        sample.split(',').map(|cube_count| {
            let (count, color) = cube_count.trim().split_once(' ').unwrap();
            let count: u32 = count.parse().unwrap();
            (color.to_string(), count)
        }).collect()
    }).collect();

    Game { id, samples }
}

fn main() {
    let limits: HashMap<String, u32> = HashMap::from([
        ("red".to_string(), 12),
        ("green".to_string(), 13),
        ("blue".to_string(), 14)
    ]);

    let result: u32 = stdin().lines().flatten()
        .map(parse_game)
        .filter(|game| {
            game.samples.iter().all(|sample| {
                sample.iter().all(|(color, count)| {
                    limits.get(color).unwrap() >= count
                })
            })
        })
        .map(|game| game.id)
        .sum();
    println!("{result}");
}
