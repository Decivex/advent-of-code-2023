use std::collections::HashMap;
use std::io::stdin;

#[derive(Debug)]
struct Game {
    id: u32,
    samples: Vec<HashMap<String, u32>>
}

fn parse_game(line: String) -> Game {
    let (id, data) = line.strip_prefix("Game ").unwrap().split_once(':').unwrap();
    let id: u32 = id.parse().unwrap();

    let samples: Vec<HashMap<String, u32>> = data.split(';').map(|sample| {
        sample.split(',').map(|cube_count| {
            let (count, color) = cube_count.trim().split_once(' ').unwrap();
            let count: u32 = count.parse().unwrap();
            (color.to_string(), count)
        }).collect()
    }).collect();

    Game { id, samples }
}

fn main() {
    let result: u32 = stdin().lines().flatten()
        .map(parse_game)
        .map(|game| {
            let mut max_cubes: HashMap<String, u32> = HashMap::new();

            for sample in game.samples {
                for (color, count) in sample {
                    let current_count = max_cubes.get(&color).unwrap_or(&0);
                    max_cubes.insert(color, *current_count.max(&count));
                }
            }

            max_cubes
        })
        .map(|max_cubes| max_cubes.values().product::<u32>())
        .sum();
    println!("{result}");
}
