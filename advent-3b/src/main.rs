use std::collections::HashSet;
use std::io::stdin;
use std::ops::Range;
use crate::Element::{Number, Symbol};

#[derive(Debug, PartialEq, Copy, Clone)]
enum Element {
    Number(u32),
    Symbol(char)
}

type Schematic = Vec<Vec<(Range<usize>, Element)>>;

type Coords = (usize, usize);

fn parse_number(slice: &str) -> Option<(usize, Element)> {
    let number: String = slice.chars().take_while(|c| c.is_ascii_digit()).collect();
    if number.is_empty() {
        None
    } else {
        Some((number.len(), Number(number.parse::<u32>().unwrap())))
    }
}

fn parse_symbol(slice: &str) -> Option<(usize, Element)> {
    let c = slice.chars().next().unwrap();
    if c != '.' {
        Some((1, Symbol(c)))
    } else {
        None
    }
}

fn scan_line<S: AsRef<str>>(line: S) -> Vec<(Range<usize>, Element)> {
    let line = line.as_ref();
    let mut offset = 0;
    let mut elements: Vec<(Range<usize>,Element)> = Vec::new();

    while offset < line.len() {
        let slice = &line[offset..];
        offset += if let Some((len, element)) = parse_number(slice).or_else(|| parse_symbol(slice)) {
            elements.push((offset..offset+len, element));
            len
        } else {
            1
        }
    }

    elements
}

fn get_adjacent_coords(row: usize, range: &Range<usize>) -> Vec<Coords> {
    let mut coords = Vec::new();

    // Start and end column for rows of coords
    let start_col = range.start.saturating_sub(1);
    let end_col = range.end + 1;

    if row > 0 {
        for c in start_col..end_col {
            coords.push((c, row - 1));
        }
    }

    if range.start > 0 {
        coords.push((range.start - 1, row));
    }

    coords.push((range.end, row));

    for c in start_col..end_col {
        coords.push((c, row + 1));
    }

    coords
}

fn find_element_at_coords(schematic: &Schematic, coords: Coords) -> Option<(usize, Range<usize>, Element)> {
    schematic.get(coords.1)
        .map(|row| row.iter()
            .find(|(range, _)| range.contains(&coords.0))
            .map(|(range, element)| (coords.1, range.clone(), *element)))
        .flatten()
}

fn main() {
    let schematic: Schematic = stdin().lines().flatten()
        .map(scan_line)
        .collect();

    let mut sum = 0;

    for (row_idx, row) in schematic.iter().enumerate() {
        for (range, element) in row.iter() {
            if element == &Symbol('*') {
                let part_numbers: HashSet<(usize, Range<usize>, u32)> = get_adjacent_coords(row_idx, range).iter()
                    .filter_map(|c| match find_element_at_coords(&schematic, *c) {
                        Some((row, range, Number(n))) => {
                            Some((row, range, n))
                        },
                        _ => None
                    })
                    .collect();
                if part_numbers.len() == 2 {
                    sum += part_numbers.iter().map(|(_, _, n)| n).product::<u32>();
                }

            }
        }
    }

    println!("{}", sum);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scan_line() {
        assert_eq!(
            super::scan_line("617*......"),
            vec![
                (0..3, Number(617)),
                (3..4, Symbol)
            ]
        )
    }
}