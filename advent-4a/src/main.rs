use std::collections::BTreeSet;
use std::io::stdin;

fn parse_list(s: &str) -> BTreeSet<u32> {
    s.trim().split_whitespace().map(|n| n.parse::<u32>().unwrap()).collect()
}

fn main() {
    let result: u32 = stdin().lines().flatten()
        .map(|line| {
            let (winning, card) = line
                .split_once(":").unwrap().1
                .split_once("|").unwrap();

            let winning = parse_list(winning);
            let card = parse_list(card);

            (1 << winning.intersection(&card).count()) >> 1
        })
        .sum();
    println!("{}", result);
}
