use std::collections::BTreeSet;
use std::io::stdin;

fn parse_list(s: &str) -> BTreeSet<u32> {
    s.trim().split_whitespace().map(|n| n.parse::<u32>().unwrap()).collect()
}

#[derive(Debug)]
struct Card {
    points: usize,
    count: usize
}

fn main() {
    let mut cards: Vec<Card> = stdin().lines().flatten()
        .map(|line| {
            let (winning, card) = line
                .split_once(":").unwrap().1
                .split_once("|").unwrap();

            let winning = parse_list(winning);
            let card = parse_list(card);
            Card {
                points: winning.intersection(&card).count(),
                count: 1
            }
        }).collect();

    let l = cards.len();
    for idx in 0..l {
        let card = cards.get(idx).unwrap();
        let copy_cards = card.points;
        let repeat = card.count;

        for c in &mut cards[idx+1..(idx+1+ copy_cards).min(l)] {
            c.count += repeat;
        }
    }

    println!("{}", cards.iter().map(|c| c.count).sum::<usize>());
}
