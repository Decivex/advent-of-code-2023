use std::collections::BTreeMap;
use std::io::{Read, stdin};

#[derive(Debug)]
struct Mapping {
    name: String,
    range_offsets: BTreeMap<i64, (i64, i64)>
}

impl Mapping {
    fn map_number(&self, input: i64) -> i64 {
        let offset = self.range_offsets.range(..=input)
            .last()
            .filter(|&(start, (end, _))| (start..end).contains(&&input))
            .map(|(_, (_, offset))| *offset)
            .unwrap_or_default();
        return input + offset;
    }
}

fn parse_mapping(text: &str) -> Mapping {
    let mut lines = text.lines();
    let name = lines.next().unwrap()
        .strip_suffix(" map:").unwrap()
        .replace('-', " ");

    let range_offsets: BTreeMap<i64, (i64, i64)> = lines.map(|line| {
        let mut split = line.split_whitespace();
        let target: i64 = split.next().unwrap().parse().unwrap();
        let start: i64 = split.next().unwrap().parse().unwrap();
        let length: i64 = split.next().unwrap().parse().unwrap();
        let offset = target - start;
        let end = start + length;

        (start, (end, offset))
    }).collect();

    Mapping { name, range_offsets }
}

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    let parts: Vec<_> = input.split("\n\n").collect();

    let seeds: Vec<i64> = parts.first()
        .unwrap()
        .strip_prefix("seeds: ")
        .unwrap()
        .split_ascii_whitespace()
        .map(|s| s.parse().unwrap())
        .collect();

    let mappings = &parts[1..].iter()
        .map(|p| parse_mapping(p))
        .collect::<Vec<_>>();

    let result = seeds.into_iter().map(|seed| {
        let mut loc = seed;

        for mapping in mappings {
            loc = mapping.map_number(loc);
        }

        loc
    }).min().unwrap();

    println!("{result}");
}
