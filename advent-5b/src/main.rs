use std::collections::BTreeMap;
use std::io::{Read, stdin};
use std::ops::Range;

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    let parts: Vec<_> = input.split("\n\n").collect();

    let seed_ranges = parse_range_list(parts[0].strip_prefix("seeds: ").unwrap());

    let mappings: Vec<RangeMapping> = parts[1..].iter().map(|text| {
        parse_number_list(text.split_once('\n').unwrap().1).into()
    }).collect();

    let mut ranges = seed_ranges.clone();

    for mapping in mappings {
        ranges = ranges.iter().flat_map(|r| {
            mapping.map_range(r.clone())
        }).collect::<Vec<_>>();
    }

    let result = ranges.iter().map(|r| r.start).min().unwrap();

    println!("{result}");
}

trait Shift {
    fn shift(&self, by: i64) -> Self;
}

impl Shift for Range<i64> {
    fn shift(&self, by: i64) -> Self {
        self.start+by..self.end+by
    }
}

#[derive(Debug, Eq, PartialEq)]
struct RangeMapping {
    // A map of start indices and offsets
    mapping: BTreeMap<i64, i64>
}

impl From<Vec<i64>> for RangeMapping {
    fn from(list: Vec<i64>) -> Self {
        assert_eq!(list.len() % 3, 0);

        let mut mapping = BTreeMap::new();

        mapping.insert(0, 0);

        for slice in list.chunks_exact(3) {
            if let &[target, source, len] = slice {
                let offset = target - source;
                mapping.insert(source, offset);
                if mapping.get(&(source + len)).unwrap_or(&0) == &0 {
                    mapping.insert(source+len, 0);
                }
            } else {
                panic!("Invalid slice length!");
            }
        }

        Self { mapping }
    }
}

impl RangeMapping {
    fn map_range(&self, range: Range<i64>) -> Vec<Range<i64>> {
        let mut output_ranges: Vec<Range<i64>> = Vec::new();

        let mut offset = self.mapping.range(..=range.start)
            .last()
            .map(|(_, offset)| offset)
            .copied()
            .unwrap_or(0);

        let mut r = range.clone();

        for (cut_point, new_offset) in self.mapping.range(range) {
            if cut_point != &r.start {
                output_ranges.push((r.start..*cut_point).shift(offset))
            }

            offset = *new_offset;
            r = *cut_point..r.end;
        }

        if r.start != r.end {
            output_ranges.push(r.shift(offset))
        }

        output_ranges
    }
}

fn parse_number_list(s: &str) -> Vec<i64> {
    s.split_whitespace().map(|n| n.parse().unwrap()).collect()
}

fn parse_range_list(s: &str) -> Vec<Range<i64>> {
    parse_number_list(s).chunks_exact(2).map(|slice| {
        if let &[start, len] = slice {
            start..start+len
        } else {
            panic!("Can't parse range list, uneven number of ellements");
        }
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_number_list() {
        let list = parse_number_list("79 14 55 13");
        assert_eq!(list, vec![79, 14, 55, 13]);
    }

    #[test]
    fn test_parse_range_list() {
        let list = parse_range_list("79 14 55 13");
        assert_eq!(list, vec![79..93, 55..68]);
    }

    #[test]
    fn test_mapping_from_vec() {
        let mapping: RangeMapping = vec![
            49, 53, 8,
            0, 11, 42,
            42, 0, 7,
            57, 7, 4,
            80, 77, 3
        ].into();

        assert_eq!(mapping, RangeMapping {
            mapping: BTreeMap::from([
                (0, 42),
                (7, 50),
                (11, -11),
                (53, -4),
                (61, 0),
                (77, 3),
                (80, 0)
            ])
        })
    }

    #[test]
    fn test_map_range() {
        let mapping = RangeMapping {
            mapping: BTreeMap::from([
                (0, 8),
                (4, 0),
                (12, -5),
                (32, 6)
            ])
        };

        let ranges = mapping.map_range(3..16);

        assert_eq!(ranges, vec![
            11..12,
            4..12,
            7..11
        ]);
    }
}