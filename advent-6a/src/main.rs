use std::io::stdin;


fn parse_number_list(s: &str) -> Vec<i64> {
    s.trim().split_whitespace().map(|n| n.parse().unwrap()).collect()
}

fn solve_quadratic(a: f32, b: f32, c: f32) -> Option<(f32, f32)> {
    let discriminant = b.powf(2.0) - 4.0 * a * c;

    if discriminant < 0.0 {
        return None;
    }

    return Some((
        (-b + discriminant.sqrt()) / (2.0 * a),
        (-b - discriminant.sqrt()) / (2.0 * a)
    ));
}

fn main() {
    let mut lines  = stdin().lines();
    let times = parse_number_list(&lines.next().unwrap().unwrap().split_once(':').unwrap().1);
    let distances = parse_number_list(&lines.next().unwrap().unwrap().split_once(':').unwrap().1);

    let result: i64 = times.into_iter().zip(distances.into_iter()).map(|(time, distance)| {
        match solve_quadratic(-1.0, time as f32, -distance as f32) {
            None => 0,
            Some((min, max)) => {
                let min = (min + 0.0001).ceil() as i64;
                let max = (max - 0.0001).floor() as i64;
                max - min + 1
            }
        }
    }).product();

    println!("{result}");
}
