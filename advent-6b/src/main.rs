use std::io::stdin;

fn solve_quadratic(a: f32, b: f32, c: f32) -> Option<(f32, f32)> {
    let discriminant = b.powf(2.0) - 4.0 * a * c;

    if discriminant < 0.0 {
        return None;
    }

    return Some((
        (-b + discriminant.sqrt()) / (2.0 * a),
        (-b - discriminant.sqrt()) / (2.0 * a)
    ));
}

fn parse_line(line: &str) -> i64 {
    line.split_once(':').unwrap().1
        .chars().filter(|c| c.is_ascii_digit())
        .collect::<String>()
        .parse::<i64>().unwrap()
}

fn main() {
    let mut lines  = stdin().lines();
    let time = parse_line(&lines.next().unwrap().unwrap());
    let distance = parse_line(&lines.next().unwrap().unwrap());

    let (min, max) = solve_quadratic(-1.0, time as f32, -distance as f32).unwrap();
    let min = (min + 0.1).ceil() as i64;
    let max = (max - 0.1).floor() as i64;
    let result = max - min;

    println!("{result}");
}
