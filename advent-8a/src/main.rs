use std::collections::HashMap;
use std::io::stdin;

#[derive(Debug)]
enum Direction {
    Left, Right
}

impl TryFrom<char> for Direction {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'L' => Ok(Direction::Left),
            'R' => Ok(Direction::Right),
            _ => Err(())
        }
    }
}

type NodeList = HashMap<String, (String, String)>;

fn parse_nodelist_line(s: String) -> (String, (String, String)) {
    let node_name = &s[0..3];
    let left = &s[7..10];
    let right = &s[12..15];
    (node_name.to_string(), (left.to_string(), right.to_string()))
}

fn main() {
    let mut lines = stdin().lines().flatten();

    let directions: Vec<Direction> = lines.next().unwrap().chars().map(Direction::try_from).flatten().collect();
    lines.next().unwrap();

    let node_list: NodeList = lines.map(parse_nodelist_line).collect();

    let mut current_node: String = "AAA".to_string();

    for (step, direction) in directions.iter().cycle().enumerate() {
        if current_node == "ZZZ" {
            println!("{step}");
            break;
        }

        let node = node_list.get(&current_node).unwrap();

        current_node = match direction {
            Direction::Left => node.0.to_string(),
            Direction::Right => node.1.to_string()
        };

    }
}
