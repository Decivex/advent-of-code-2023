use std::collections::HashMap;
use std::fmt::Debug;
use std::io::stdin;
use std::str::FromStr;

#[derive(Debug)]
enum Direction {
    Left, Right
}

impl TryFrom<char> for Direction {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'L' => Ok(Direction::Left),
            'R' => Ok(Direction::Right),
            _ => Err(())
        }
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Hash)]
struct Node(char, char, char);

impl Node {
    fn node_type(&self) -> NodeType {
        match self.2 {
            'A' => NodeType::Start,
            'Z' => NodeType::End,
            _ => NodeType::Normal
        }
    }
}

impl FromStr for Node {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        Ok(Node(chars.next().ok_or(())?, chars.next().ok_or(())?, chars.next().ok_or(())?))
    }
}

#[derive(Eq, PartialEq)]
enum NodeType {
    Start,
    End,
    Normal
}

type NodeList = HashMap<Node, (Node, Node)>;

fn parse_nodelist_line(s: String) -> Result<(Node, (Node, Node)), ()> {
    let node = Node::from_str(&s[0..3])?;
    let left = Node::from_str(&s[7..10])?;
    let right = Node::from_str(&s[12..15])?;
    Ok((node, (left, right)))
}

fn main() {
    let mut lines = stdin().lines().flatten();

    let directions: Vec<Direction> = lines.next().unwrap().chars().map(Direction::try_from).flatten().collect();
    lines.next().unwrap();

    let node_list: NodeList = lines.map(parse_nodelist_line).flatten().collect();

    let result = node_list.keys()
        .filter(|n| n.node_type() == NodeType::Start)
        .map(|n| path_length(&node_list, n, &directions))
        .reduce(num::integer::lcm)
        .unwrap();

    println!("{result}");
}

fn path_length(node_list: &NodeList, start_node: &Node, directions: &Vec<Direction>) -> usize {
    let mut node = start_node;

    for (step, direction) in directions.iter().cycle().enumerate() {
        if node.node_type() == NodeType::End {
            return step;
        }

        let (l, r) = node_list.get(&node).unwrap();
        let next = &match direction {
            Direction::Left => l,
            Direction::Right => r
        };

        node = *next;
    }

    panic!("No path found!");
}