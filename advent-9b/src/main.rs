use std::io::stdin;
use std::iter::successors;

fn parse_list(s: String) -> Vec<i64> {
    s.split_whitespace().map(|n| n.parse().unwrap()).collect()
}

fn derivative(list: &Vec<i64>) -> Vec<i64> {
    list.windows(2).map(|s| s[1] - s[0]).collect()
}

fn get_derivatives(list: Vec<i64>) -> Vec<Vec<i64>> {
    successors(Some(list), |l| {
        if l.iter().all(|n| *n == 0) {
            None
        } else {
            Some(derivative(l))
        }
    }).collect()
}

fn main() {
    let result: i64 = stdin().lines().flatten().map(parse_list).map(|sequence| {
        get_derivatives(sequence).iter()
            .map(|d| *d.first().unwrap())
            .rev()
            .reduce(|a, b| b - a).unwrap()
    }).sum();
    println!("{result}");
}
